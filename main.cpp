#include <iostream>
#include <cstdio>
#include <time.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include <fstream>
#include <vector>

void ShowConsoleCursor(bool showFlag)
{
    HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

    CONSOLE_CURSOR_INFO     cursorInfo;

    GetConsoleCursorInfo(out, &cursorInfo);
    cursorInfo.bVisible = showFlag; // set the cursor visibility
    SetConsoleCursorInfo(out, &cursorInfo);
}

void setCursorPosition(int x, int y)
{
    static const HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    std::cout.flush();
    COORD coord = { (SHORT)x, (SHORT)y };
    SetConsoleCursorPosition(hOut, coord);
}

void cls()
{
    // Get the Win32 handle representing standard output.
    // This generally only has to be done once, so we make it static.
    static const HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);

    CONSOLE_SCREEN_BUFFER_INFO csbi;
    COORD topLeft = { 0, 0 };

    // std::cout uses a buffer to batch writes to the underlying console.
    // We need to flush that to the console because we're circumventing
    // std::cout entirely; after we clear the console, we don't want
    // stale buffered text to randomly be written out.
    std::cout.flush();

    // Figure out the current width and height of the console window
    if (!GetConsoleScreenBufferInfo(hOut, &csbi)) {
        // TODO: Handle failure!
        abort();
    }
    DWORD length = csbi.dwSize.X * csbi.dwSize.Y;

    DWORD written;

    // Flood-fill the console with spaces to clear it
    FillConsoleOutputCharacter(hOut, TEXT(' '), length, topLeft, &written);

    // Reset the attributes of every character to the default.
    // This clears all background colour formatting, if any.
    FillConsoleOutputAttribute(hOut, csbi.wAttributes, length, topLeft, &written);

    // Move the cursor back to the top left for the next sequence of writes
    SetConsoleCursorPosition(hOut, topLeft);
}

class Player
{
public:
	int X=19,Y=9;
	int ogonki = 5;

	void wLewo()
	{
	    X--;
	}
	void wPrawo()
	{
	    X++;
	}

	void wGore()
	{
	    Y--;
	}
	void wDol()
	{
	    Y++;
	}
};
struct Ogonek
{
    int X,Y;
};
class Jablko
{
public:
    int X, Y;


    void losuj ()
    {
        X=rand()%40;
        Y=rand()%20;
    }
};

int main()
{

    ShowConsoleCursor(false);
	char plansza[40][20];
	char input;
	std::vector <Ogonek> wektorOgonow ;
	Ogonek ogonek;
	char last='w';
	Player* wskaznik;
	Player player;
	Jablko apple;
	wskaznik=&player;
	bool czyNiePrzegral=true;
	char interfejs;
	char zapisac;
	std::string nick;
	std::string dane;
	std::fstream plik;
//
    apple.losuj();

while (true)
{
    cls();
    printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%cSNAKE%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n\n",176,176,176,176,176,177,177,177,177,177,178,178,178,178,178,178,178,178,178,178,177,177,177,177,177,176,176,176,176,176);
    printf("Rozpocznij gre:             kliknij 1\nWyjdz z gry:                kliknij 2\nWyswietl tablice wynikow:   kliknij 3\n\n\n\n\nSterowanie:\n\nPoruszanie: 'w' 'a' 's' 'd' \nWyjscie do menu w trakcie gry: 'x'");
    interfejs = _getch();

    switch (interfejs)
    {
    case '1':
    czyNiePrzegral=true;
    wektorOgonow.push_back(ogonek);
    cls();
    ///////////////////////////////////////////////////tworzenie planszy
            for (int i=0;i<20;i++)
            {
                for (int j=0;j<40;j++)
                {
				plansza[i][j] = '-';
                }
            }
/////////////////////////////////////////////////// rysowanie planszy

			for (int i = 0; i < 20; i++)
			{
				for (int j = 0; j < 40; j++)
				{
					printf("%c", plansza[i][j]);
				}
				printf("\n");
			}
		while (czyNiePrzegral)
		{

/////////////////////////ogonkow
                wektorOgonow[0].X=player.X;
                wektorOgonow[0].Y=player.Y;
            if (player.ogonki!=0)
            {

                for (int i=player.ogonki; i>0;i--)
                {
                    wektorOgonow[i].X=wektorOgonow[i-1].X;
                    wektorOgonow[i].Y=wektorOgonow[i-1].Y;
                }
            }
            setCursorPosition(wektorOgonow[player.ogonki].X,wektorOgonow[player.ogonki].Y);
            std::cout << "-";
/////////////////////////////////////////////////// sterowanie graczem//gracz
            if (kbhit())
            {
            input = _getch();
            switch (input)
            {
                case 'w':
                    if (last=='s'){wskaznik->wDol();break;}
                    wskaznik->wGore();
                    last='w';
                    break;
                case 's':
                    if (last=='w'){wskaznik->wGore();break;}
                    wskaznik->wDol();
                    last='s';
                    break;
                case 'd':
                    if (last=='a'){wskaznik->wLewo();break;}
                    wskaznik->wPrawo();
                    last='d';
                    break;
                case 'a':
                    if (last=='d'){wskaznik->wPrawo();break;}
                    wskaznik->wLewo();
                    last='a';
                    break;
                    case 'x': czyNiePrzegral=true;break;
                default:continue;
            }
            }
            else
            {
                switch (last)
            {
                case 'w':
                    wskaznik->wGore();
                    break;
                case 's':
                    wskaznik->wDol();
                    break;
                case 'd':
                    wskaznik->wPrawo();
                    break;
                case 'a':
                    wskaznik->wLewo();
                    break;
                default:break;
            }
            }
            if (player.X>=40)
            {
                player.X=0;
            }
            if (player.Y>=20)
            {
                player.Y=0;
            }
            if (player.X<0)
            {
                player.X=39;
            }
            if (player.Y<0)
            {
                player.Y=19;
            }


            setCursorPosition(player.X, player.Y);
            std::cout << "o";


            if ((apple.X==player.X)&&(apple.Y==player.Y))
            {

                apple.losuj();
                for (int i=0;i<player.ogonki;i++)
                {
                    if ((apple.X==wektorOgonow[i].X)&&(apple.Y==wektorOgonow[i].Y))
                    {
                        apple.losuj();
                        i=0;
                    }
                }

                player.ogonki++;
                setCursorPosition(40,19);
                std::cout<<player.ogonki-5;
            }
                        setCursorPosition(apple.X, apple.Y);
            std::cout << "x";
            for (int l=0;l<player.ogonki;l++)
            {
                if  ((player.X==wektorOgonow[l].X)&&(player.Y==wektorOgonow[l].Y))
                    czyNiePrzegral=false;
            }


////////////////////////////////////////////////////wpierdol jablko na plansze

            plansza[apple.X][apple.Y] ='x';



////////////////////////////////////////////////////restartowanie
			Sleep(130);

		}
		cls();
		printf("Och losie! Przegrales!\nUdalo Ci sie zjesc %i jabluszek.\nCzy chcesz zapisac swoj wynik? y/n \n",player.ogonki-5);

		std::cin>>zapisac;
		switch (zapisac)
		{
		case 'y':
        {
        plik.open("highscores.txt",std::ios::app);

        if (plik.good()==true)
        {
            std::cout<<"Podaj swoj nick"<<std::endl;
            std::cin>>nick;
            plik<<nick<<" "<<player.ogonki-1<<std::endl;
            plik.close();
        }
        else {std::cout<<"Nie uzyskano dostepu do pliku!";getchar();getchar();};
        }
        case 'n': break;
       // default: continue;
		}

        player.ogonki=5;

		break;
		case '2':return 0;
		case '3':
		    cls();
        plik.open("highscores.txt",std::ios::in);
        if (plik.good()==true)
        {
            while (getline(plik,dane))
            {

                std::cout<<dane<<std::endl;
            }
            plik.close();
            printf("\nWcisnij dowolny przycisk by kontynuowac");
            getch();

        }
        else {std::cout<<"Nie uzyskano dostepu do pliku!";getchar();};
        break;
        default: {cls();break;}
    }

}

    return 0;
}

